export function protectLocalCA(){
    if(!process.env.NODE_EXTRA_CA_CERTS) return;

    const https = require('https');
    const tls = require('tls');
    const fs = require('fs');
    
    const list = (process.env.NODE_EXTRA_CA_CERTS || '').split(',');
    const additionalCerts = list.map(extraCert => fs.readFileSync(extraCert, 'utf8'));
    
    https.globalAgent.options.ca = [
        ...tls.rootCertificates,
        ...additionalCerts
    ];
}

