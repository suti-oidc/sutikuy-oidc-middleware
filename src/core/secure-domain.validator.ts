export class SecureDomainValidator{
    constructor(){}

    ensure(domain: string | null): string | null{
        return !domain ? domain : domain.indexOf('http://') < 0 &&  domain.indexOf('https://') < 0 ? 'https://' + domain : domain.replace('http://','https://');
    }

}