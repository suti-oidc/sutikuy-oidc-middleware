import { auth } from "express-oauth2-jwt-bearer";
import { Handler } from "express";
import { protectLocalCA } from "./core/local-issuer.protector";
import { SecureDomainValidator } from "./core/secure-domain.validator";

export function sutikuy(issuer: string = process.env.IDENTITY_ISSUER, baseUrl: string = process.env.IDENTITY_BASE_URL): Handler{
    const secureDomainValidator = new SecureDomainValidator();
    issuer = secureDomainValidator.ensure(issuer);
    baseUrl =secureDomainValidator.ensure(baseUrl);
    if (process.env.NODE_ENV == 'local'){
        protectLocalCA();
    }
    return auth({
        issuerBaseURL: `${issuer}/identity/.well-known/openid-configuration`,
        audience: baseUrl
    });        
}