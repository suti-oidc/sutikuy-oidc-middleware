import { SecureDomainValidator } from "../../../src/core/secure-domain.validator";

describe('IssuerValidor',() => {
    let secureDomainValidator: SecureDomainValidator;
    beforeEach(() => {
        secureDomainValidator = new SecureDomainValidator();
    });

    describe('ensure', () => {

        it('Should be null when it doesnt have URL', () => {
            expect(secureDomainValidator.ensure(null)).toBeNull();
            expect(secureDomainValidator.ensure(undefined)).toBeUndefined();
        });

        it('Should be add https:// when has empty protocol', () => {
            expect(secureDomainValidator.ensure('hola.com.pe')).toBe('https://hola.com.pe');
        });

        it('Should replace http:// by https://', () => {
            expect(secureDomainValidator.ensure('http://hola.com.pe')).toBe('https://hola.com.pe');
        });
    });
});
